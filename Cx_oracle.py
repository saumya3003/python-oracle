#==============================================================================
# Connecting to Oracle and executing a query
#==============================================================================

import cx_Oracle as co
con=co.Connection("system/oracle@localhost/xe")
cur=co.Cursor(con)
cur.execute("select * from employees")
print(cur.rowcount)
con.commit()
cur.close()
con.close()

#==============================================================================
# Printing the values retrieved from select query
#==============================================================================

import cx_Oracle
con=cx_Oracle.Connection("system/oracle@localhost/xe")
cur=cx_Oracle.Cursor(con)
try:
    cur.execute("select * from employees")
    for ROW in cur:          #iT WILL GIVE VALUES OF ALL COLUMN AS A TUPLE
        print(ROW)		           iT WILL PRINT VALUES OF ALL COLUMN AS A TUPLE
        print(ROW[1],ROW[2], ROW[5])   #iT WILL PRINT VALUES INDIVIDUAL VALUES
    con.commit()
except cx_Oracle.DatabaseError as e:
    print(e)
finally:
	cur.close()
	con.close()
	
#==============================================================================
#printing particular fields
#==============================================================================

import cx_Oracle
con=cx_Oracle.Connection("system/oracle@localhost/xe")
cur=cx_Oracle.Cursor(con)
try:
    cur.execute("select EMPLOYEE_ID, First_name from employees")
    for id,ename in cur: #---id,ename are python variabl;e and not column name
        print(id)
    con.commit()
except cx_Oracle.DatabaseError as e:
	print(e)
finally:
	cur.close()
	con.close()
       
#==============================================================================
#Hard Pass
#==============================================================================

import cx_Oracle
con=cx_Oracle.Connection("system/oracle@localhost/xe")
cur=cx_Oracle.Cursor(con)
list_of_employee=[123,124,205,100]
for i in list_of_employee:
    cur.execute("select EMPLOYEE_ID, First_name from employees where EMPLOYEE_ID="+str(i))
    for ROW in cur:
        print(ROW)
cur.close()
con.close()

#==============================================================================
# Soft pass---query will be compiled only ones
#==============================================================================

import cx_Oracle
con=cx_Oracle.Connection("system/oracle@localhost/xe")
cur=cx_Oracle.Cursor(con)
list_of_employee=[123,124,205,100]
for i in list_of_employee:
    cur.execute("select EMPLOYEE_ID, First_name from employees where EMPLOYEE_ID=:emp_id",{"emp_id":i})
    for ROW in cur:
        print(ROW)
cur.close()
con.close()

#==============================================================================
#Multiple Bind Parameters
#==============================================================================

import cx_Oracle
con=cx_Oracle.Connection("system/oracle@localhost/xe")
cur=cx_Oracle.Cursor(con)
list_of_employee=[123,124,205,100]
for i in list_of_employee:
    cur.execute("select EMPLOYEE_ID, First_name from employees where EMPLOYEE_ID=:emp_id1 or EMPLOYEE_ID=:emp_id2 or EMPLOYEE_ID=:emp_id3",{"emp_id1":100,"emp_id2":101,"emp_id3":200})
    for ROW in cur:
        print(ROW)
cur.close()
con.close()

#==============================================================================
#DML Statement
#==============================================================================


import cx_Oracle as co
con=co.Connection("system/oracle@localhost/xe")
cur=co.Cursor(con)
cur.execute("insert into dummy values (2,'b')")
print(cur.rowcount)
con.commit()
cur.close()
con.close()